package com.example.salespromotionrest.utils;

import com.example.salespromotionrest.entity.Discount;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
public class CartUtils {

    public BigDecimal calculateLineDiscount(BigDecimal totalPrice, Discount discountPromotion) {
        return totalPrice.multiply(discountPromotion.getDiscount()).divide(BigDecimal.valueOf(100), RoundingMode.valueOf(2)).min(discountPromotion.getDiscountLimit());
    }

}
