package com.example.salespromotionrest.utils;

import org.apache.commons.beanutils.BeanUtilsBean;

import java.lang.reflect.InvocationTargetException;

public class NullAwareBeanUtilsBean extends BeanUtilsBean {

    // copy non-null property, id excluded
    @Override
    public void copyProperty(Object dest, String name, Object value) throws IllegalAccessException, InvocationTargetException {
        if (value == null || name.equals("id")) return;
        super.copyProperty(dest, name, value);
    }

}
