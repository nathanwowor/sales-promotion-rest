package com.example.salespromotionrest.exception;

import com.example.salespromotionrest.entity.Promotion;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class PromotionOutOfDateException extends RuntimeException {

    public PromotionOutOfDateException(Promotion promotion) {
        super(promotion.getDocumentNumber() + " has been changed. Re-apply promotions before checkout.");
    }

}
