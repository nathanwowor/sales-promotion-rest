package com.example.salespromotionrest.exception;

import com.example.salespromotionrest.entity.Cart;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CartAlreadyPaidException extends RuntimeException {

    public CartAlreadyPaidException(Cart cart) {
        super("Operation not permitted for paid cart: " + cart.getDocumentNumber());
    }

}
