package com.example.salespromotionrest.service;

import com.example.salespromotionrest.dto.ProductDto;
import com.example.salespromotionrest.repository.ProductRepo;
import com.example.salespromotionrest.utils.JwtUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepo productRepo;
    private final JwtUtils jwtUtils;

    @GetMapping
    public List<ProductDto> getAllProducts(String auth) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        return productRepo.findAllByUserId(userId);
    }

}
