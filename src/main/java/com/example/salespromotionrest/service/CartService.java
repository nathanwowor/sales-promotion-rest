package com.example.salespromotionrest.service;

import com.example.salespromotionrest.dto.CartAndItemsDto;
import com.example.salespromotionrest.entity.Cart;
import com.example.salespromotionrest.entity.CartItem;
import com.example.salespromotionrest.entity.Discount;
import com.example.salespromotionrest.entity.FreeGoods;
import com.example.salespromotionrest.entity.Product;
import com.example.salespromotionrest.entity.Promotion;
import com.example.salespromotionrest.entity.User;
import com.example.salespromotionrest.exception.CartAlreadyPaidException;
import com.example.salespromotionrest.exception.OutOfStockException;
import com.example.salespromotionrest.exception.PromotionOutOfDateException;
import com.example.salespromotionrest.repository.CartItemRepo;
import com.example.salespromotionrest.repository.CartRepo;
import com.example.salespromotionrest.repository.ProductRepo;
import com.example.salespromotionrest.repository.PromotionRepo;
import com.example.salespromotionrest.utils.CartUtils;
import com.example.salespromotionrest.utils.JwtUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class CartService {

    private final CartRepo cartRepo;
    private final CartItemRepo cartItemRepo;
    private final ProductRepo productRepo;
    private final PromotionRepo promotionRepo;
    private final CartUtils cartUtils;
    private final JwtUtils jwtUtils;

    public Page<Cart> getCarts(String auth, Optional<Integer> page, Optional<String> query) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        Pageable pageable = PageRequest.of(page.orElse(1) - 1, 10, Sort.by(Sort.Direction.DESC, "document_number"));
        if (query.isPresent()) {
            return cartRepo.findAllByUserIdAndDocumentNumber(userId, query.get().trim(), pageable);
        } else {
            return cartRepo.findAllByUserId(userId, pageable);
        }
    }

    public List<CartItem> getCartItems(String auth, String documentNumber) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        // check if cart belongs to requesting user
        Cart cart = cartRepo.findByUserIdAndDocumentNumber(userId, documentNumber).orElseThrow();
        return cartItemRepo.findAllByCart(cart);
    }

    public void addCart(String auth, CartAndItemsDto dto) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        // assign user to cart
        dto.getCart().setUser(new User(userId));
        dto.getCart().setDateCreated(new Date());
        saveCart(dto.getCart(), dto.getCartItems());
    }

    public Cart updateCart(String auth, String documentNumber, CartAndItemsDto dto) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        Cart cart = cartRepo.findByUserIdAndDocumentNumber(userId, documentNumber).orElseThrow();
        // reject update if cart already paid
        if (cart.getIsPaid()) throw new CartAlreadyPaidException(cart);
        // update cart description
        cart.setDescription(dto.getCart().getDescription());
        // reset previously saved cart items
        cartItemRepo.deleteAllByCart(cart);
        return saveCart(cart, dto.getCartItems());
    }

    public Cart saveCart(Cart cart, List<CartItem> items) {
        BigDecimal totalGrossPrice = BigDecimal.ZERO;
        for (CartItem item : items) {
            item.setCart(cart); // assign cart to items
            // update product to the latest version
            Product product = productRepo.getByUserIdAndCode(cart.getUser().getId(), item.getProduct().getCode());
            item.setProduct(product);
            // calculate total gross price
            totalGrossPrice = totalGrossPrice.add(product.getPrice().multiply(BigDecimal.valueOf(item.getQuantity())));
        }
        cart.setTotalGrossPrice(totalGrossPrice);
        cart.setTotalLineDiscount(BigDecimal.ZERO);
        cartItemRepo.saveAll(items);
        return cartRepo.save(cart);
    }

    public Cart applyPromotions(String auth, String documentNumber) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        Cart cart = cartRepo.findByUserIdAndDocumentNumber(userId, documentNumber).orElseThrow();
        // reject update if cart already paid
        if (cart.getIsPaid()) throw new CartAlreadyPaidException(cart);
        boolean hasPromotion = false;
        List<CartItem> items = cartItemRepo.findAllByCart(cart);
        // apply the best available promo to each item
        BigDecimal totalLineDiscount = BigDecimal.valueOf(0);
        for (CartItem item : items) {
            item.setPromotion(null); // reset item promo
            // get list of all applicable promo
            BigDecimal totalPrice = item.getProduct().getPrice().multiply(BigDecimal.valueOf(item.getQuantity()));
            List<Promotion> promotions = promotionRepo.findAllByProductCodeAndPurchasePriceAndValid(userId, item.getProduct().getCode(), totalPrice);
            // get list of free goods promo with enough stock
            List<Promotion> freeGoodsList = promotions.stream().filter(promotion -> {
                if (!FreeGoods.class.equals(promotion.getClass())) return false;
                FreeGoods freeGoods = (FreeGoods) promotion;
                Integer productStock = productRepo.getStockByUserIdAndCode(userId, freeGoods.getFreeProductCode());
                Optional<CartItem> cartItem = items.stream().filter(i -> i.getProduct().getCode().equals(freeGoods.getFreeProductCode())).findFirst();
                if (cartItem.isEmpty()) return productStock - freeGoods.getFreeProductQuantity() >= 0;
                return productStock - cartItem.get().getQuantity() - freeGoods.getFreeProductQuantity() >= 0;
            }).toList();
            // if free goods promotion(s) exist, apply free goods with the best value
            // if >1 best value, use the earliest promo end date
            if (!freeGoodsList.isEmpty()) {
                FreeGoods bestFreeGoods = (FreeGoods) freeGoodsList.stream()
                        .max(Comparator.comparing((Promotion promotion) -> {
                            FreeGoods freeGoodsPromotion = (FreeGoods) promotion;
                            Product product = productRepo.getByUserIdAndCode(userId, freeGoodsPromotion.getFreeProductCode());
                            return product.getPrice().multiply(BigDecimal.valueOf(freeGoodsPromotion.getFreeProductQuantity()));
                        }).thenComparing(Comparator.comparing(Promotion::getEndDate).reversed())).get();
                item.setPromotion(bestFreeGoods);
                hasPromotion = true;
            } else {
                // else try to get discount with the best value
                // if >1 best value, use the earliest promo end date
                Discount bestDiscount = (Discount) promotions.stream()
                        .filter(promotion -> Discount.class.equals(promotion.getClass()))
                        .max(Comparator.comparing((Promotion promotion) -> cartUtils.calculateLineDiscount(totalPrice, (Discount) promotion))
                                .thenComparing(Comparator.comparing(Promotion::getEndDate).reversed()))
                        .orElse(null);
                if (bestDiscount != null) {
                    item.setPromotion(bestDiscount);
                    totalLineDiscount = totalLineDiscount.add(cartUtils.calculateLineDiscount(totalPrice, bestDiscount));
                    hasPromotion = true;
                }
            }
        }
        if (!hasPromotion) return null;
        cart.setTotalLineDiscount(totalLineDiscount);
        cartItemRepo.saveAll(items);
        return cartRepo.save(cart);
    }

    public void checkoutCart(String auth, String documentNumber) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        Cart cart = cartRepo.findByUserIdAndDocumentNumber(userId, documentNumber).orElseThrow();
        // reject update if cart already paid
        if (cart.getIsPaid()) throw new CartAlreadyPaidException(cart);
        List<CartItem> items = cartItemRepo.findAllByCart(cart);
        for (CartItem item : items) {
            // check if promo is still available and up-to-date
            if (item.getPromotion() != null) {
                Optional<Promotion> promotion = promotionRepo.findByUserIdAndDocumentNumber(userId, item.getPromotion().getDocumentNumber());
                if (promotion.isEmpty() || !item.getPromotion().getId().equals(promotion.get().getId())) {
                    throw new PromotionOutOfDateException(item.getPromotion());
                }
            }
            // check and update product stocks
            Product product = productRepo.getByUserIdAndCode(userId, item.getProduct().getCode());
            if (product.getStock() < item.getQuantity()) throw new OutOfStockException(product.getName());
            productRepo.updateStockById(product.getId(), item.getQuantity());
            // check and update free product stock
            if (item.getPromotion() != null && item.getPromotion().getClass().equals(FreeGoods.class)) {
                FreeGoods freeGoods = (FreeGoods) item.getPromotion();
                Product freeProduct = productRepo.getByUserIdAndCode(userId, freeGoods.getFreeProductCode());
                // check if cart has same product as free goods promo
                Optional<CartItem> cartItem = items.stream().filter(i -> i.getProduct().getCode().equals(freeGoods.getFreeProductCode())).findFirst();
                if (cartItem.isPresent() && freeProduct.getStock() - cartItem.get().getQuantity() - freeGoods.getFreeProductQuantity() < 0) {
                    throw new OutOfStockException(freeProduct.getName() + " (FREE)");
                } else if (freeProduct.getStock() - freeGoods.getFreeProductQuantity() < 0) {
                    throw new OutOfStockException(freeProduct.getName() + " (FREE)");
                }
                productRepo.updateStockById(freeProduct.getId(), freeGoods.getFreeProductQuantity());
            }
        }
        cartRepo.setCartIsPaidTrue(cart.getId());
    }

    public void deleteCart(String auth, String documentNumber) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        Cart deletedCart = cartRepo.findByUserIdAndDocumentNumber(userId, documentNumber).orElseThrow();
        if (deletedCart.getIsPaid()) throw new CartAlreadyPaidException(deletedCart);
        deletedCart.setIsActive(null);
        cartRepo.save(deletedCart);
    }

}
