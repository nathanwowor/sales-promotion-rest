package com.example.salespromotionrest.service;

import com.example.salespromotionrest.entity.Discount;
import com.example.salespromotionrest.entity.FreeGoods;
import com.example.salespromotionrest.entity.Promotion;
import com.example.salespromotionrest.entity.User;
import com.example.salespromotionrest.repository.PromotionRepo;
import com.example.salespromotionrest.utils.JwtUtils;
import com.example.salespromotionrest.utils.NullAwareBeanUtilsBean;
import lombok.RequiredArgsConstructor;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class PromotionService {

    private final BeanUtilsBean beanUtilsBean = new NullAwareBeanUtilsBean();
    private final PromotionRepo promotionRepo;
    private final JwtUtils jwtUtils;

    public Page<Promotion> getPromotions(String auth, Optional<Integer> page, Optional<String> filter, Optional<String> query) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        Pageable pageable = PageRequest.of(page.orElse(1) - 1, 10, Sort.by(Sort.Direction.DESC, "document_number"));
        if (filter.isPresent() && query.isPresent()) {
            switch (filter.get()) {
                case "Document Number":
                    return promotionRepo.findAllByUserIdAndDocumentNumber(userId, query.get(), pageable);
                case "Product Code":
                    return promotionRepo.findAllByUserIdAndForPurchaseProductCode(userId, query.get(), pageable);
            }
        }
        return promotionRepo.findAllByUserId(userId, pageable);
    }

    public void addPromotion(String auth, Promotion promotion) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        // assign user to promotion
        promotion.setUser(new User(userId));
        promotion.setDateCreated(new Date());
        promotionRepo.save(promotion);
    }

    public Promotion updatePromotion(String auth, String documentNumber, Promotion promotion) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        Promotion currentPromotion = promotionRepo.findByUserIdAndDocumentNumber(userId, documentNumber).orElseThrow();
        Promotion updatedPromotion; // new object needed to update class type
        if (FreeGoods.class.equals(promotion.getClass())) {
            updatedPromotion = new FreeGoods();
        } else {
            updatedPromotion = new Discount();
        }
        // copy all non-null properties except id
        try {
            beanUtilsBean.copyProperties(updatedPromotion, currentPromotion);
            beanUtilsBean.copyProperties(updatedPromotion, promotion);
        } catch (IllegalAccessException | InvocationTargetException ignored) {
        }
        promotionRepo.softDeleteById(currentPromotion.getId());
        return promotionRepo.save(updatedPromotion);
    }

    public void deletePromotion(String auth, String documentNumber) {
        Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        Promotion deletedPromotion = promotionRepo.findByUserIdAndDocumentNumber(userId, documentNumber).orElseThrow();
        promotionRepo.softDeleteById(deletedPromotion.getId());
    }

}
