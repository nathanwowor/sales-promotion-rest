package com.example.salespromotionrest.service;

import com.example.salespromotionrest.entity.User;
import com.example.salespromotionrest.repository.UserRepo;
import com.example.salespromotionrest.utils.JwtUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepo userRepo;
    private final JwtUtils jwtUtils;

    public void register(User user) {
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        userRepo.save(user);
    }

    public ResponseEntity<String> login(User user) {
        Optional<User> existingUser = userRepo.findByEmail(user.getEmail());
        if (existingUser.isEmpty()) return new ResponseEntity<>("Email not registered.", HttpStatus.NOT_FOUND);

        boolean passwordMatch = BCrypt.checkpw(user.getPassword().getBytes(StandardCharsets.UTF_8), existingUser.get().getPassword());
        if (!passwordMatch) return new ResponseEntity<>("Wrong password.", HttpStatus.UNAUTHORIZED);

        return new ResponseEntity<>(jwtUtils.generateToken(existingUser.get()), HttpStatus.OK);
    }

}
