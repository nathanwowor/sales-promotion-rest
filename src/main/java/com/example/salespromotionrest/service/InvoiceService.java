package com.example.salespromotionrest.service;

import com.example.salespromotionrest.entity.Cart;
import com.example.salespromotionrest.entity.CartItem;
import com.example.salespromotionrest.entity.Discount;
import com.example.salespromotionrest.entity.FreeGoods;
import com.example.salespromotionrest.entity.Product;
import com.example.salespromotionrest.entity.Promotion;
import com.example.salespromotionrest.repository.CartItemRepo;
import com.example.salespromotionrest.repository.CartRepo;
import com.example.salespromotionrest.repository.ProductRepo;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InvoiceService {

    private final CartRepo cartRepo;
    private final CartItemRepo cartItemRepo;
    private final ProductRepo productRepo;
    // private final JwtUtils jwtUtils;

    public String formatPrice(BigDecimal price) {
        return String.format("%.2f", price)
                .replaceAll("\\.", ",")
                .replaceAll("\\B(?=(\\d{3})+(?!\\d))", ".");
    }

    // public void printPdfInvoice(String auth, Long id, HttpServletResponse response) {
    public void printPdfInvoice(String documentNumber, HttpServletResponse response) {
        // get cart data
        // Long userId = jwtUtils.getIdFromToken(auth.substring(7));
        Cart cart = cartRepo.findByDocumentNumber(documentNumber).orElseThrow();
        List<CartItem> items = cartItemRepo.findAllByCart(cart);
        Long userId = cart.getUser().getId();

        // generate pdf
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=" + documentNumber + "_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);
        try {
            Document document = new Document(PageSize.A4);
            PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
            // PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("HelloWorld.pdf"));
            document.open();

            Font fontTitle = FontFactory.getFont(FontFactory.COURIER_BOLD);
            Font font = FontFactory.getFont(FontFactory.COURIER_BOLD);
            fontTitle.setSize(20);

            document.add(new Paragraph("FAKTUR NO. : " + documentNumber.replace("FAKTUR", ""), font));

            Instant instant = Instant.ofEpochMilli(cart.getDateCreated().getTime());
            LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.ofHours(7));
            String orderDate = ldt.toLocalDate().toString();
            String orderTime = ldt.toLocalTime().toString().split("\\.")[0];
            document.add(new Paragraph("TANGGAL    : " + orderDate + " " + orderTime, font));

            PdfPTable table = new PdfPTable(5);
            table.setWidthPercentage(100f);
            // table.setWidths(new float[]{3.5f, 3.0f, 3.0f});
            table.setSpacingBefore(10);

            PdfPCell headerCell = new PdfPCell();
            headerCell.setPadding(5);
            headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            headerCell.setPhrase(new Phrase("PRODUK", font));
            table.addCell(headerCell);
            headerCell.setPhrase(new Phrase("JUMLAH", font));
            table.addCell(headerCell);
            headerCell.setPhrase(new Phrase("HARGA", font));
            table.addCell(headerCell);
            headerCell.setPhrase(new Phrase("DISKON", font));
            table.addCell(headerCell);
            headerCell.setPhrase(new Phrase("NET", font));
            table.addCell(headerCell);
            PdfPCell dataCell = new PdfPCell();
            dataCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            for (CartItem item : items) {
                table.addCell(new Phrase(item.getProduct().getName().toUpperCase(), font));
                dataCell.setPhrase(new Phrase(item.getQuantity().toString(), font));
                table.addCell(dataCell);
                dataCell.setPhrase(new Phrase("Rp " + formatPrice(item.getProduct().getPrice()), font));
                table.addCell(dataCell);
                BigDecimal grossPrice = item.getProduct().getPrice().multiply(BigDecimal.valueOf(item.getQuantity()));
                Promotion promotion = item.getPromotion();
                if (promotion != null && Discount.class.equals(promotion.getClass())) {
                    Discount discountPromotion = (Discount) promotion;
                    dataCell.setPhrase(new Phrase(formatPrice(discountPromotion.getDiscount()) + "%", font));
                    table.addCell(dataCell);
                    BigDecimal lineDiscount = grossPrice
                            .multiply(discountPromotion.getDiscount())
                            .divide(BigDecimal.valueOf(100), RoundingMode.valueOf(2))
                            .min(discountPromotion.getDiscountLimit());
                    BigDecimal netPrice = grossPrice.subtract(lineDiscount);
                    dataCell.setPhrase(new Phrase("Rp " + formatPrice(netPrice), font));
                    table.addCell(dataCell);
                } else {
                    dataCell.setPhrase(new Phrase("0,00%", font));
                    table.addCell(dataCell);
                    dataCell.setPhrase(new Phrase("Rp " + formatPrice(grossPrice), font));
                    table.addCell(dataCell);
                    if (promotion != null) {
                        FreeGoods freeGoodsPromotion = (FreeGoods) promotion;
                        Product freeProduct = productRepo.getByUserIdAndCode(userId, freeGoodsPromotion.getFreeProductCode());
                        table.addCell(new Phrase(freeProduct.getName().toUpperCase() + " (FREE)", font));
                        dataCell.setPhrase(new Phrase(freeGoodsPromotion.getFreeProductQuantity().toString(), font));
                        table.addCell(dataCell);
                        dataCell.setPhrase(new Phrase("Rp " + formatPrice(BigDecimal.ZERO), font));
                        table.addCell(dataCell);
                        dataCell.setPhrase(new Phrase("0,00%", font));
                        table.addCell(dataCell);
                        dataCell.setPhrase(new Phrase("Rp " + formatPrice(BigDecimal.ZERO), font));
                        table.addCell(dataCell);
                    }
                }
            }
            PdfPCell longCell = new PdfPCell();
            longCell.setColspan(4);
            longCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            longCell.setPhrase(new Phrase("SUBTOTAL", font));
            table.addCell(longCell);
            dataCell.setPhrase(new Phrase("Rp " + formatPrice(cart.getTotalGrossPrice()), font));
            table.addCell(dataCell);
            longCell.setPhrase(new Phrase("DISKON", font));
            table.addCell(longCell);
            dataCell.setPhrase(new Phrase("Rp " + formatPrice(cart.getTotalLineDiscount()), font));
            table.addCell(dataCell);
            longCell.setPhrase(new Phrase("NET TOTAL", font));
            table.addCell(longCell);
            dataCell.setPhrase(new Phrase("Rp " + formatPrice(cart.getTotalGrossPrice().subtract(cart.getTotalLineDiscount())), font));
            table.addCell(dataCell);
            document.add(table);

            document.close();
            writer.close();
        } catch (DocumentException | IOException ignored) {
        }
    }

}
