package com.example.salespromotionrest.controller;

import com.example.salespromotionrest.dto.CartAndItemsDto;
import com.example.salespromotionrest.entity.Cart;
import com.example.salespromotionrest.entity.CartItem;
import com.example.salespromotionrest.service.CartService;
import com.example.salespromotionrest.service.InvoiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("carts")
@RequiredArgsConstructor
public class CartController {

    private final CartService cartService;
    private final InvoiceService invoiceService;

    @GetMapping
    public Page<Cart> getCarts(@RequestHeader("Authorization") String auth, @RequestParam Optional<Integer> page, @RequestParam Optional<String> query) {
        return cartService.getCarts(auth, page, query);
    }

    @GetMapping("{documentNumber}/items")
    public List<CartItem> getCartItems(@RequestHeader("Authorization") String auth, @PathVariable String documentNumber) {
        return cartService.getCartItems(auth, documentNumber);
    }

    @GetMapping("{documentNumber}/pdf")
    // public void printPdfInvoice(@RequestHeader("Authorization") String auth, @PathVariable String documentNumber, HttpServletResponse response) {
    public void printPdfInvoice(@PathVariable String documentNumber, HttpServletResponse response) {
        // invoiceService.printPdfInvoice(auth, documentNumber, response);
        invoiceService.printPdfInvoice(documentNumber, response);
    }

    @PostMapping
    public void addCart(@RequestHeader("Authorization") String auth, @RequestBody @Valid CartAndItemsDto dto) {
        cartService.addCart(auth, dto);
    }

    @PutMapping("{documentNumber}")
    public Cart updateCart(@RequestHeader("Authorization") String auth, @PathVariable String documentNumber, @RequestBody @Valid CartAndItemsDto dto) {
        return cartService.updateCart(auth, documentNumber, dto);
    }

    @PutMapping("{documentNumber}/promotions")
    public Cart applyPromotions(@RequestHeader("Authorization") String auth, @PathVariable String documentNumber) {
        return cartService.applyPromotions(auth, documentNumber);
    }

    @PutMapping("{documentNumber}/checkout")
    public void checkoutCart(@RequestHeader("Authorization") String auth, @PathVariable String documentNumber) {
        cartService.checkoutCart(auth, documentNumber);
    }

    @DeleteMapping("{documentNumber}")
    public void deleteCart(@RequestHeader("Authorization") String auth, @PathVariable String documentNumber) {
        cartService.deleteCart(auth, documentNumber);
    }

}
