package com.example.salespromotionrest.controller;

import com.example.salespromotionrest.entity.User;
import com.example.salespromotionrest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("register")
    public void register(@RequestBody @Valid User user) {
        userService.register(user);
    }

    @PostMapping("login")
    public ResponseEntity<String> login(@RequestBody User user) {
        return userService.login(user);
    }

}
