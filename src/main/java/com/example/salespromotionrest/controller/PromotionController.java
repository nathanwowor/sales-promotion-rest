package com.example.salespromotionrest.controller;

import com.example.salespromotionrest.entity.Promotion;
import com.example.salespromotionrest.service.PromotionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("promotions")
@RequiredArgsConstructor
public class PromotionController {

    private final PromotionService promotionService;

    @GetMapping
    public Page<Promotion> getPromotions(@RequestHeader("Authorization") String auth, @RequestParam Optional<Integer> page, @RequestParam Optional<String> filter, @RequestParam Optional<String> query) {
        return promotionService.getPromotions(auth, page, filter, query);
    }

    @PostMapping
    public void addPromotion(@RequestHeader("Authorization") String auth, @RequestBody @Valid Promotion promotion) {
        promotionService.addPromotion(auth, promotion);
    }

    @PutMapping("{documentNumber}")
    public Promotion updatePromotion(@RequestHeader("Authorization") String auth, @PathVariable String documentNumber, @RequestBody Promotion promotion) {
        return promotionService.updatePromotion(auth, documentNumber, promotion);
    }

    @DeleteMapping("{documentNumber}")
    public void deletePromotion(@RequestHeader("Authorization") String auth, @PathVariable String documentNumber) {
        promotionService.deletePromotion(auth, documentNumber);
    }

}
