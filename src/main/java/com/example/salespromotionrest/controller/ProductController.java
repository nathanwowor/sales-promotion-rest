package com.example.salespromotionrest.controller;

import com.example.salespromotionrest.dto.ProductDto;
import com.example.salespromotionrest.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping
    public List<ProductDto> getAllProducts(@RequestHeader("Authorization") String auth) {
        return productService.getAllProducts(auth);
    }

}
