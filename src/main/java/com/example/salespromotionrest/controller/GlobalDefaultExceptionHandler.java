package com.example.salespromotionrest.controller;

import com.example.salespromotionrest.exception.CartAlreadyPaidException;
import com.example.salespromotionrest.exception.OutOfStockException;
import com.example.salespromotionrest.exception.PromotionOutOfDateException;
import io.jsonwebtoken.JwtException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

@RestControllerAdvice
public class GlobalDefaultExceptionHandler {

    @Data
    @AllArgsConstructor
    public static class ErrorResponseBody {

        private String code;
        private String message;
        private String path;

    }

    @ExceptionHandler(CartAlreadyPaidException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorResponseBody handleCartAlreadyPaid(CartAlreadyPaidException exception, HttpServletRequest request) {
        return new ErrorResponseBody("CART_ALREADY_PAID", exception.getMessage(), request.getRequestURI());
    }

    @ExceptionHandler(OutOfStockException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorResponseBody handleOutOfStock(OutOfStockException exception, HttpServletRequest request) {
        return new ErrorResponseBody("OUT_OF_STOCK", exception.getMessage(), request.getRequestURI());
    }

    @ExceptionHandler(PromotionOutOfDateException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorResponseBody handlePromotionOutOfDate(PromotionOutOfDateException exception, HttpServletRequest request) {
        return new ErrorResponseBody("PROMOTION_OUT_OF_DATE", exception.getMessage(), request.getRequestURI());
    }

    @ExceptionHandler(NoSuchElementException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponseBody handleEntityNotFound(NoSuchElementException exception, HttpServletRequest request) {
        return new ErrorResponseBody("NOT_FOUND", exception.getMessage(), request.getRequestURI());
    }

    @ExceptionHandler(JwtException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorResponseBody handleInvalidToken(JwtException exception, HttpServletRequest request) {
        return new ErrorResponseBody("INVALID_TOKEN", exception.getMessage(), request.getRequestURI());
    }

    // handle failed @Valid check
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, String> handleInvalidRequest(MethodArgumentNotValidException exception) {
        Map<String, String> messages = new HashMap<>();
        for (FieldError error : exception.getFieldErrors()) {
            messages.put(error.getField(), error.getDefaultMessage());
        }
        return messages;
    }

}
