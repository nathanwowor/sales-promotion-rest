package com.example.salespromotionrest.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        // why bcrypt: https://auth0.com/blog/hashing-in-action-understanding-bcrypt/
        return new BCryptPasswordEncoder();
    }

}
