package com.example.salespromotionrest.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @ManyToOne
    @JsonIgnore
    private User user;

    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateCreated;

    @Column(length = 20, updatable = false)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String documentNumber;

    @NotNull
    @Size(max = 255, message = "Description can't exceed 255 characters")
    private String description;

    @PositiveOrZero
    @Column(columnDefinition = "DECIMAL(20,4) UNSIGNED DEFAULT 0")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private BigDecimal totalGrossPrice;

    @PositiveOrZero
    @Column(columnDefinition = "DECIMAL(20,4) UNSIGNED DEFAULT 0", insertable = false)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private BigDecimal totalLineDiscount;

    @Column(columnDefinition = "BOOLEAN DEFAULT 0", insertable = false)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Boolean isPaid;

    @Column(columnDefinition = "BOOLEAN DEFAULT 1 CHECK (is_active > 0)", insertable = false)
    @JsonIgnore
    private Boolean isActive;
    // true (default) = active
    // check: false not allowed
    // null = soft delete

}
