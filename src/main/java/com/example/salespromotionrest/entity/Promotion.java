package com.example.salespromotionrest.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING, length = 10)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Discount.class, name = "DISCOUNT"),
        @JsonSubTypes.Type(value = FreeGoods.class, name = "FREE_GOODS")
})
@Data
@NoArgsConstructor
public class Promotion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @ManyToOne
    @JsonIgnore
    private User user;

    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date dateCreated;

    @Column(length = 20)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String documentNumber;

    @NotNull
    @Size(max = 255, message = "Description can't exceed 255 characters")
    private String description;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate endDate;

    @NotNull
    @Size(max = 20)
    @Column(length = 20)
    private String forPurchaseProductCode;

    @NotNull
    @PositiveOrZero
    @Column(columnDefinition = "DECIMAL(20,4) UNSIGNED")
    private BigDecimal forPurchasePrice;

    @NotNull
    @Column(length = 10, insertable = false, updatable = false)
    private String type;

    @Column(columnDefinition = "BOOLEAN DEFAULT 1 CHECK (is_active > 0)", insertable = false)
    @JsonIgnore
    private Boolean isActive;
    // true (default) = active
    // check: false not allowed
    // null = soft delete

}
