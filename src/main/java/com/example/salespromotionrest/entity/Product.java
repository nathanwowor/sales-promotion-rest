package com.example.salespromotionrest.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"user_id", "code", "isActive"})})
@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @ManyToOne
    @JsonIgnore
    private User user;

    @NotNull
    @Size(max = 20)
    @Column(length = 20)
    private String code;

    @Size(max = 50)
    @Column(length = 50)
    private String name;

    @PositiveOrZero
    @Column(columnDefinition = "DECIMAL(20,4) UNSIGNED")
    private BigDecimal price;

    @PositiveOrZero
    @Column(columnDefinition = "INT UNSIGNED")
    private Integer stock;

    @Column(columnDefinition = "BOOLEAN DEFAULT 1 CHECK (is_active > 0)", insertable = false)
    @JsonIgnore
    private Boolean isActive;
    // true (default) = active
    // check: false not allowed
    // null = soft delete

}
