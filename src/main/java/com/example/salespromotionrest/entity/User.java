package com.example.salespromotionrest.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Data
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @NotBlank(message = "Email address required")
    @Pattern(regexp = "^\\w+([\\-_+.]\\w+)*@\\w+([\\-_+.]\\w+)*(\\.\\w{2,3})+$", message = "Invalid email address")
    @Column(unique = true, nullable = false)
    private String email;

    @NotBlank(message = "Password required")
    @Size(min = 8, message = "Password must be at least 8 characters")
    @Size(max = 60, message = "Password can't exceed 60 characters") // bcrypt max = 72
    @Column(columnDefinition = "VARBINARY(60)", nullable = false) // size for bcrypt-encoded password
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    public User(Long id) {
        this.id = id;
    }

}
