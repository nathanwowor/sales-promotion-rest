package com.example.salespromotionrest.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Entity
@DiscriminatorValue("FREE_GOODS")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class FreeGoods extends Promotion {

    @NotNull
    @Size(max = 20)
    @Column(length = 20)
    private String freeProductCode;

    @NotNull
    @PositiveOrZero
    @Column(columnDefinition = "INT UNSIGNED")
    private Integer freeProductQuantity;

    // private Boolean isRepeated;

}
