package com.example.salespromotionrest.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Entity
@DiscriminatorValue("DISCOUNT")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class Discount extends Promotion {

    @NotNull
    @PositiveOrZero
    @DecimalMax("80.0")
    @Column(columnDefinition = "DECIMAL(4,2) UNSIGNED CHECK (discount <= 80)")
    private BigDecimal discount;

    @NotNull
    @PositiveOrZero
    @Column(columnDefinition = "DECIMAL(20,4) UNSIGNED")
    private BigDecimal discountLimit;

}
