package com.example.salespromotionrest.repository;

import com.example.salespromotionrest.entity.Cart;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CartRepo extends JpaRepository<Cart, Long> {

    @Query(nativeQuery = true, value = "" +
            "SELECT * FROM cart " +
            "WHERE is_active = TRUE " +
            "AND UPPER(document_number) = UPPER(?1)")
    Optional<Cart> findByDocumentNumber(String documentNumber);

    @Query(nativeQuery = true, value = "" +
            "SELECT * FROM cart " +
            "WHERE is_active = TRUE " +
            "AND user_id = ?1 " +
            "AND UPPER(document_number) = UPPER(?2)")
    Optional<Cart> findByUserIdAndDocumentNumber(Long userId, String documentNumber);

    @Query(nativeQuery = true, countQuery = "SELECT COUNT(*) FROM cart WHERE user_id = ?1", value = "" +
            "SELECT id, date_created, document_number, description, total_gross_price, total_line_discount, is_paid, NULL AS is_active, NULL AS user_id " +
            "FROM cart " +
            "WHERE is_active = TRUE AND user_id = ?1")
    Page<Cart> findAllByUserId(Long userId, Pageable pageable);

    @Query(nativeQuery = true, countQuery = "SELECT COUNT(*) FROM cart WHERE is_active = true AND user_id = ?1 AND UPPER(document_number) LIKE CONCAT('%', UPPER(?2), '%')", value = "" +
            "SELECT id, date_created, document_number, description, total_gross_price, total_line_discount, is_paid, NULL AS is_active, NULL AS user_id " +
            "FROM cart " +
            "WHERE is_active = TRUE AND user_id = ?1 " +
            "AND UPPER(document_number) LIKE CONCAT('%', UPPER(?2), '%')")
    Page<Cart> findAllByUserIdAndDocumentNumber(Long userId, String documentNumber, Pageable pageable);

    @Modifying
    @Query(nativeQuery = true, value = "" +
            "UPDATE cart " +
            "SET is_paid = true " +
            "WHERE id = ?1")
    void setCartIsPaidTrue(Long id);

}
