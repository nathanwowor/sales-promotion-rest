package com.example.salespromotionrest.repository;

import com.example.salespromotionrest.entity.Promotion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface PromotionRepo extends JpaRepository<Promotion, Long> {

    @Query(nativeQuery = true, value = "" +
            "SELECT * FROM promotion " +
            "WHERE is_active = TRUE AND user_id = ?1 " +
            "AND UPPER(document_number) = UPPER(?2)")
    Optional<Promotion> findByUserIdAndDocumentNumber(Long userId, String documentNumber);

    @Query(nativeQuery = true, countQuery = "SELECT COUNT(*) FROM promotion WHERE is_active = TRUE AND user_id = ?1", value = "" +
            "SELECT type, id, date_created, document_number, description, start_date, end_date, for_purchase_product_code, for_purchase_price, discount, discount_limit, free_product_code, free_product_quantity, NULL AS is_active, NULL AS user_id " +
            "FROM promotion " +
            "WHERE is_active = TRUE AND user_id = ?1")
    Page<Promotion> findAllByUserId(Long userId, Pageable pageable);

    @Query(nativeQuery = true, countQuery = "SELECT COUNT(*) FROM promotion WHERE is_active = TRUE AND user_id = ?1 AND UPPER(document_number) LIKE CONCAT('%', UPPER(?2), '%')", value = "" +
            "SELECT type, id, date_created, document_number, description, start_date, end_date, for_purchase_product_code, for_purchase_price, discount, discount_limit, free_product_code, free_product_quantity, NULL AS is_active, NULL AS user_id " +
            "FROM promotion " +
            "WHERE is_active = TRUE AND user_id = ?1 " +
            "AND UPPER(document_number) LIKE CONCAT('%', UPPER(?2), '%')")
    Page<Promotion> findAllByUserIdAndDocumentNumber(Long userId, String documentNumber, Pageable pageable);

    @Query(nativeQuery = true, countQuery = "SELECT COUNT(*) FROM promotion WHERE is_active = TRUE AND user_id = ?1 AND UPPER(for_purchase_product_code) LIKE CONCAT('%', UPPER(?2), '%')", value = "" +
            "SELECT type, id, date_created, document_number, description, start_date, end_date, for_purchase_product_code, for_purchase_price, discount, discount_limit, free_product_code, free_product_quantity, NULL AS is_active, NULL AS user_id " +
            "FROM promotion " +
            "WHERE is_active = TRUE AND user_id = ?1 " +
            "AND UPPER(for_purchase_product_code) LIKE CONCAT('%', UPPER(?2), '%')")
    Page<Promotion> findAllByUserIdAndForPurchaseProductCode(Long userId, String productCode, Pageable pageable);

    @Query(nativeQuery = true, value = "" +
            // SQL statements for JPA must select all columns
            "SELECT type, id, discount, discount_limit, free_product_code, free_product_quantity, end_date, NULL AS date_created, NULL AS document_number, NULL AS description, NULL AS start_date, NULL AS for_purchase_product_code, NULL AS for_purchase_price, NULL AS is_active, NULL AS user_id " +
            "FROM promotion " +
            "WHERE user_id = ?1 AND " +
            "is_active IS NOT NULL " +
            "AND UPPER(for_purchase_product_code) LIKE CONCAT('%', UPPER(?2), '%') " +
            "AND ?3 >= for_purchase_price " +
            "AND DATE(CONVERT_TZ(UTC_TIMESTAMP(), '+00:00' ,'+07:00')) BETWEEN start_date AND end_date")
    List<Promotion> findAllByProductCodeAndPurchasePriceAndValid(Long userId, String productCode, BigDecimal totalPrice);
    // bug: using method name without native query returns empty list on 1st request, throws exception on 2nd request

    @Modifying
    @Query(nativeQuery = true, value = "" +
            "UPDATE promotion " +
            "SET is_active = NULL " +
            "WHERE id = ?1")
    void softDeleteById(Long id);

}
