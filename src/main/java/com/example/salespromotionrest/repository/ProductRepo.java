package com.example.salespromotionrest.repository;

import com.example.salespromotionrest.dto.ProductDto;
import com.example.salespromotionrest.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepo extends JpaRepository<Product, Long> {

    @Query(nativeQuery = true, value = "" +
            "SELECT code, name, price, stock " +
            "FROM product " +
            "WHERE is_active = true AND user_id = ?1")
    List<ProductDto> findAllByUserId(Long userId);

    @Query(nativeQuery = true, value = "" +
            "SELECT id, code, name, price, stock, NULL AS is_active, NULL AS user_id " +
            "FROM product " +
            "WHERE is_active = true AND user_id = ?1 " +
            "AND UPPER(code) = UPPER(?2)")
    Product getByUserIdAndCode(Long userId, String code);

    @Query(nativeQuery = true, value = "" +
            "SELECT stock FROM product " +
            "WHERE is_active = TRUE AND user_id = ?1 " +
            "AND UPPER(code) = UPPER(?2)")
    Integer getStockByUserIdAndCode(Long userId, String code);

    @Modifying
    @Query(nativeQuery = true, value = "" +
            "UPDATE product " +
            "SET stock = stock - ?2 " +
            "WHERE id = ?1")
    void updateStockById(Long id, Integer stock);

}
