package com.example.salespromotionrest.repository;

import com.example.salespromotionrest.entity.Cart;
import com.example.salespromotionrest.entity.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartItemRepo extends JpaRepository<CartItem, Long> {

    List<CartItem> findAllByCart(Cart cart);

    void deleteAllByCart(Cart cart);

}
