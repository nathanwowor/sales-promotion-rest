package com.example.salespromotionrest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public interface PromotionDto {

    Date getDateCreated();

    String getDocumentNumber();

    String getDescription();

    LocalDate getStartDate();

    LocalDate getEndDate();

    String getForPurchaseProductCode();

    BigDecimal getForPurchasePrice();

    String getType();

    BigDecimal getDiscount();

    BigDecimal getDiscountLimit();

    String getFreeProductCode();

    Integer getFreeProductQuantity();

}
