package com.example.salespromotionrest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public interface ProductDto {

    String getCode();

    String getName();

    BigDecimal getPrice();

    Integer getStock();

}
