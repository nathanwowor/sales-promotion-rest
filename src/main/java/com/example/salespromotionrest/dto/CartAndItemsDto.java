package com.example.salespromotionrest.dto;

import com.example.salespromotionrest.entity.Cart;
import com.example.salespromotionrest.entity.CartItem;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class CartAndItemsDto {

    @Valid Cart cart;
    List<@Valid CartItem> cartItems;

}
