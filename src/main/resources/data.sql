CREATE TRIGGER sales_promotion.generate_cart_number
    BEFORE INSERT
    ON sales_promotion.cart
    FOR EACH ROW SET
    @auto_id = (SELECT IFNULL(MAX(id), 0) + 1
                FROM sales_promotion.cart),
    NEW.document_number = IFNULL(NEW.document_number, CONCAT('FAKTUR', LPAD(@auto_id, 3, '0')));

CREATE TRIGGER sales_promotion.generate_promotion_number
    BEFORE INSERT
    ON sales_promotion.promotion
    FOR EACH ROW SET
    @auto_id = (SELECT IFNULL(MAX(id), 0) + 1
                FROM sales_promotion.promotion),
    NEW.document_number = IFNULL(NEW.document_number, CONCAT('PROMO', LPAD(@auto_id, 3, '0')));

INSERT INTO sales_promotion.user (email, password)
VALUES ('user@example.com', '$2a$10$uu/sI7qgXZ.GbKxDXyDWMegjg5r3Ah.LJE8xhwU5dc1zdVzX8lksK'),
       ('user2@example.com', '$2a$10$uu/sI7qgXZ.GbKxDXyDWMegjg5r3Ah.LJE8xhwU5dc1zdVzX8lksK');

INSERT INTO sales_promotion.product (user_id, code, name, price, stock)
VALUES (1, 'PRODUK001', 'Cola', '5000', 10),
       (1, 'PRODUK002', 'Milk', '7000', 10),
       (1, 'PRODUK003', 'Chocolate', '12000', 10),
       (1, 'PRODUK004', 'Potato Chips', '20000', 10),
       (2, 'PRODUK001', 'Cola v2.0', '5000', 50),
       (2, 'PRODUK002', 'Milk v2.0', '7000', 50),
       (2, 'PRODUK003', 'Chocolate v2.0', '12000', 50),
       (2, 'PRODUK004', 'Potato Chips v2.0', '20000', 50);

INSERT INTO sales_promotion.promotion (user_id, start_date, end_date, description, for_purchase_product_code, for_purchase_price, type, discount, discount_limit)
VALUES (1, '2022-05-15', '2022-07-01', 'buy cola Rp 10k get 10% disc max Rp 20k', 'PRODUK001', 10000, 'DISCOUNT', 10, 20000),
       (1, '2022-05-01', '2022-07-01', 'buy cola Rp 20k get 20% disc max Rp 10k', 'PRODUK001', 20000, 'DISCOUNT', 20, 10000),
       (1, '2022-04-15', '2022-06-15', 'buy cola Rp 20k get 20% disc max Rp 10k', 'PRODUK001', 20000, 'DISCOUNT', 20, 10000),
       (1, '2022-04-01', '2022-06-01', 'buy milk Rp 20k get 10% disc max Rp 10k', 'PRODUK002', 20000, 'DISCOUNT', 10, 10000),
       (1, '2022-03-15', '2022-05-15', 'buy chocolate Rp 100k get 5% disc max Rp 20k', 'PRODUK003', 100000, 'DISCOUNT', 5, 20000),
       (1, '2022-03-01', '2022-05-01', 'buy potato chips Rp 40k get 5% disc max Rp 10k', 'PRODUK004', 40000, 'DISCOUNT', 5, 10000);
INSERT INTO sales_promotion.promotion (user_id, start_date, end_date, description, for_purchase_product_code, for_purchase_price, type, free_product_code, free_product_quantity)
VALUES (1, '2022-01-01', '2022-06-01', 'buy milk Rp 50k get 1 free milk', 'PRODUK002', 50000, 'FREE_GOODS', 'PRODUK002', 2),
       (1, '2022-01-01', '2022-06-01', 'buy chocolate Rp 20k get 1 free chocolate', 'PRODUK003', 20000, 'FREE_GOODS', 'PRODUK003', 1),
       (1, '2022-01-01', '2022-06-01', 'buy potato chips Rp 50k get 1 free cola', 'PRODUK004', 50000, 'FREE_GOODS', 'PRODUK001', 1);

INSERT INTO sales_promotion.cart (user_id, description, total_gross_price, is_paid)
VALUES (1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 72000, 1),
       (1, 'In nec consequat eros.', 20000, 1),
       (1, 'Donec scelerisque tempor ultrices.', 45000, 1),
       (1, 'Nulla vitae lectus quam.', 24000, 1),
       (1, 'Suspendisse nec augue suscipit, condimentum lectus a, sodales felis.', 20000, 1),
       (1, 'Nullam pharetra libero non quam mollis, sit amet rutrum lacus maximus.', 43000, 1),
       (1, 'Ut dapibus nibh at consectetur euismod.', 24000, 1),
       (1, 'Praesent sollicitudin massa viverra lacus bibendum, in venenatis orci elementum.', 125000, 1),
       (1, 'Nam varius mi nisi.', 48000, 0),
       (1, 'Suspendisse cursus cursus mattis.', 30000, 0),
       (1, 'Nullam suscipit magna tincidunt, aliquet nisl sed, egestas dolor.', 69000, 0),
       (1, 'Donec ornare dui in nulla rutrum bibendum a ut mi.', 75000, 0),
       (1, 'Nullam laoreet euismod porttitor.', 28000, 0);
INSERT INTO sales_promotion.cart_item (cart_id, product_id, quantity)
VALUES (1, 1, 3),
       (1, 2, 3),
       (1, 3, 3),
       (2, 4, 1),
       (3, 1, 2),
       (3, 2, 5),
       (4, 3, 2),
       (5, 4, 1),
       (6, 1, 3),
       (6, 2, 4),
       (7, 3, 2),
       (8, 4, 4),
       (8, 1, 2),
       (8, 2, 5),
       (9, 3, 4),
       (10, 4, 1),
       (10, 1, 2),
       (11, 2, 3),
       (11, 3, 4),
       (12, 4, 3),
       (12, 1, 3),
       (13, 2, 4);