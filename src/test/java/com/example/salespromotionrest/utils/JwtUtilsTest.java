package com.example.salespromotionrest.utils;

import com.example.salespromotionrest.entity.User;
import com.example.salespromotionrest.repository.UserRepo;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class JwtUtilsTest {

    @Autowired
    JwtUtils jwtUtils;
    @Autowired
    UserRepo userRepo;

    static User user;

    @BeforeAll
    static void beforeAll() {
        user = new User();
        user.setId(1L);
        user.setEmail("email@example.com");
    }

    @Test
    void generateTokenAndGetSubjectFromToken() {
        String token = jwtUtils.generateToken(user);
        Long id = jwtUtils.getIdFromToken(token);
        assertThat(id).isEqualTo(user.getId());
    }

}