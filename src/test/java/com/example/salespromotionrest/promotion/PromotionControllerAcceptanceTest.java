package com.example.salespromotionrest.promotion;

import com.example.salespromotionrest.entity.Discount;
import com.example.salespromotionrest.entity.Promotion;
import com.example.salespromotionrest.entity.User;
import com.example.salespromotionrest.repository.PromotionRepo;
import com.example.salespromotionrest.repository.UserRepo;
import com.example.salespromotionrest.utils.JwtUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
class PromotionControllerAcceptanceTest {

    @Autowired
    MockMvc mvc;
    @Autowired
    PromotionRepo promotionRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    JwtUtils jwtUtils;
    @Autowired
    JacksonTester<Promotion> promotionJacksonTester;

    User user;
    String auth;
    Promotion promotion;

    @BeforeEach
    void setUp() {
        user = new User();
        user.setEmail("user@example.com");
        user.setPassword("password"); // ignored, only for passing @Valid
        userRepo.save(user);
        auth = "Bearer " + jwtUtils.generateToken(user);

        promotion = new Discount();
        promotion.setUser(user);
        promotion.setDocumentNumber("PROMO999");
        promotion.setDescription("");
        promotion.setStartDate(LocalDate.now());
        promotion.setEndDate(LocalDate.now());
        promotion.setForPurchaseProductCode("PRODUK001");
        promotion.setForPurchasePrice(BigDecimal.ONE);
        promotion.setType("DISCOUNT");
        ((Discount) promotion).setDiscount(BigDecimal.valueOf(80));
        ((Discount) promotion).setDiscountLimit(BigDecimal.valueOf(10000));
    }

    @AfterEach
    void tearDown() {
        promotionRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    void getPromotions() throws Exception {
        // given
        promotionRepo.save(promotion);

        // then
        mvc.perform(get("/promotions")
                        .header("Authorization", auth))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id").value(promotion.getId()));
    }

    @Test
    void searchPromotionByProductCode() throws Exception {
        // given
        String productCode = "PRODUK999";
        promotion.setForPurchaseProductCode(productCode);
        promotionRepo.save(promotion);

        // then
        mvc.perform(get("/promotions?filter=Product Code&query=" + productCode)
                        .header("Authorization", auth))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].forPurchaseProductCode").value(productCode));
    }

    @Test
    void searchPromotionByDocumentNumber() throws Exception {
        // given
        promotionRepo.save(promotion);
        String documentNumber = promotion.getDocumentNumber();

        // then
        mvc.perform(get("/promotions?filter=Document Number&query=" + documentNumber)
                        .header("Authorization", auth))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].documentNumber").value(documentNumber));
    }

    @Test
    void addPromotion() throws Exception {
        mvc.perform(post("/promotions")
                        .header("Authorization", auth)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(promotionJacksonTester.write(promotion).getJson()))
                .andExpect(status().isOk());
        Optional<Promotion> savedPromotion = promotionRepo.findAll().stream().findAny();
        assertThat(savedPromotion.isPresent()).isTrue();
    }

    @Test
    void deletePromotion() throws Exception {
        // given
        promotionRepo.save(promotion);

        // then
        mvc.perform(delete("/promotions/" + promotion.getDocumentNumber())
                        .header("Authorization", auth))
                .andDo(print())
                .andExpect(status().isOk());
        assertThat(promotionRepo.findById(promotion.getId()).orElseThrow().getIsActive()).isNull();
    }

    @Test
    void updatePromotion() throws Exception {
        // given
        promotionRepo.save(promotion);
        String newDescription = "Lorem ipsum.";

        // then
        promotion.setDescription(newDescription);
        mvc.perform(put("/promotions/" + promotion.getDocumentNumber())
                        .header("Authorization", auth)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(promotionJacksonTester.write(promotion).getJson()))
                .andDo(print())
                .andExpect(status().isOk());
        assertThat(promotionRepo.findById(promotion.getId()).orElseThrow().getIsActive()).isNull();
        assertThat(promotionRepo.findByUserIdAndDocumentNumber(user.getId(), promotion.getDocumentNumber()).orElseThrow().getDescription()).isEqualTo(newDescription);
    }

}