package com.example.salespromotionrest.cart;

import com.example.salespromotionrest.dto.CartAndItemsDto;
import com.example.salespromotionrest.entity.Cart;
import com.example.salespromotionrest.entity.CartItem;
import com.example.salespromotionrest.entity.User;
import com.example.salespromotionrest.repository.CartItemRepo;
import com.example.salespromotionrest.repository.CartRepo;
import com.example.salespromotionrest.repository.UserRepo;
import com.example.salespromotionrest.entity.Product;
import com.example.salespromotionrest.repository.ProductRepo;
import com.example.salespromotionrest.utils.JwtUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
class CartControllerAcceptanceTest {

    @Autowired
    MockMvc mvc;
    @Autowired
    CartRepo cartRepo;
    @Autowired
    CartItemRepo cartItemRepo;
    @Autowired
    ProductRepo productRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    JwtUtils jwtUtils;
    @Autowired
    JacksonTester<CartAndItemsDto> dtoJacksonTester;

    User user;
    String auth;
    Product product;
    Cart cart;
    CartItem item;
    List<CartItem> items;
    CartAndItemsDto dto;

    @BeforeEach
    void setUp() {
        user = new User();
        user.setEmail("user@example.com");
        user.setPassword("password"); // ignored, only for passing @Valid
        userRepo.save(user);
        auth = "Bearer " + jwtUtils.generateToken(user);

        product = new Product();
        product.setUser(user);
        product.setName("Dummy Product");
        product.setCode("PRODUK999");
        product.setPrice(BigDecimal.ONE);
        product.setStock(1);
        productRepo.save(product);

        cart = new Cart();
        cart.setUser(user);
        cart.setDateCreated(new Date());
        cart.setDocumentNumber("FAKTUR999");
        cart.setDescription("");
        cart.setTotalGrossPrice(BigDecimal.ONE);
        cart.setTotalLineDiscount(BigDecimal.ZERO);

        item = new CartItem();
        item.setCart(cart);
        item.setProduct(product);
        item.setQuantity(1);

        items = new ArrayList<>();
        items.add(item);

        dto = new CartAndItemsDto();
        dto.setCart(cart);
        dto.setCartItems(items);
    }

    @AfterEach
    void tearDown() {
        cartRepo.deleteAll();
        cartItemRepo.deleteAll();
        productRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    void getCarts() throws Exception {
        // given
        cartRepo.save(cart);

        // then
        mvc.perform(get("/carts")
                        .header("Authorization", auth))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id").value(cart.getId()));
    }

    @Test
    void addCart() throws Exception {
        mvc.perform(post("/carts")
                        .header("Authorization", auth)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(dtoJacksonTester.write(dto).getJson()))
                .andExpect(status().isOk());
        Optional<Cart> savedCart = cartRepo.findAll().stream().findAny();
        assertThat(savedCart.isPresent()).isTrue();
        List<CartItem> savedItems = cartItemRepo.findAllByCart(savedCart.get());
        assertThat(savedItems.isEmpty()).isFalse();
    }

    @Test
    void getCartItems() throws Exception {
        // given
        cartRepo.save(cart);
        cartItemRepo.saveAll(items);

        // then
        mvc.perform(get("/carts/" + cart.getDocumentNumber() + "/items")
                        .header("Authorization", auth))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").exists());
    }

    @Test
    void updateCart() throws Exception {
        // given
        cartRepo.save(cart);
        String newDescription = "Lorem ipsum.";

        // then
        cart.setDescription(newDescription);
        mvc.perform(put("/carts/" + cart.getDocumentNumber())
                        .header("Authorization", auth)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(dtoJacksonTester.write(dto).getJson()))
                .andDo(print())
                .andExpect(status().isOk());
        assertThat(cartRepo.findById(cart.getId()).orElseThrow().getDescription()).isEqualTo(newDescription);
    }

    @Test
    void applyPromotions() throws Exception {
        // given
        cartRepo.save(cart);
        cartItemRepo.saveAll(items);

        // then
        mvc.perform(put("/carts/" + cart.getDocumentNumber() + "/promotions")
                        .header("Authorization", auth))
                .andDo(print())
                .andExpect(status().isOk());
        // todo : check if data updated correctly
    }

    @Test
    void checkoutCart() throws Exception {
        // given
        cartRepo.save(cart);
        cartItemRepo.saveAll(items);

        mvc.perform(put("/carts/" + cart.getDocumentNumber() + "/checkout")
                        .header("Authorization", auth))
                .andDo(print())
                .andExpect(status().isOk());
        assertThat(cartRepo.findById(cart.getId()).orElseThrow().getIsPaid()).isEqualTo(true);
        assertThat(productRepo.findById(product.getId()).orElseThrow().getStock()).isEqualTo(product.getStock() - item.getQuantity());
    }

    @Test
    void printPdfInvoice() throws Exception {
        // given
        cartRepo.save(cart);
        cartItemRepo.saveAll(items);

        // then
        mvc.perform(get("/carts/" + cart.getDocumentNumber() + "/pdf")
                        .header("Authorization", auth))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_PDF));
    }

    @Test
    void deleteCart() throws Exception {
        // given
        cartRepo.save(cart);

        // then
        mvc.perform(delete("/carts/" + cart.getDocumentNumber())
                        .header("Authorization", auth))
                .andDo(print())
                .andExpect(status().isOk());
        assertThat(cartRepo.findById(cart.getId()).orElseThrow().getIsActive()).isNull();
    }

}