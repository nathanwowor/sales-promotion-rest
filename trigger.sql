DROP DATABASE sales_promotion;

SHOW TABLES FROM sales_promotion;
SHOW COLUMNS FROM sales_promotion.user;
SHOW COLUMNS FROM sales_promotion.product;
SHOW COLUMNS FROM sales_promotion.cart;
SHOW COLUMNS FROM sales_promotion.cart_item;
SHOW COLUMNS FROM sales_promotion.promotion;

INSERT INTO sales_promotion.promotion (type, discount) VALUES ('DISCOUNT', 75.5);
INSERT INTO sales_promotion.product (code, is_active, price) VALUES ('B', null, 10000);
INSERT INTO sales_promotion.cart_item (quantity) VALUES (0);

SELECT * FROM sales_promotion.user;
SELECT * FROM sales_promotion.product;
SELECT * FROM sales_promotion.cart;
SELECT * FROM sales_promotion.promotion;

SHOW TRIGGERS FROM sales_promotion;

CREATE
    TRIGGER  sales_promotion.cart_document_number
 BEFORE INSERT ON sales_promotion.cart FOR EACH ROW
	SET
    @cart_id = (SELECT IFNULL(MAX(id), 0) + 1 FROM sales_promotion.cart),
    NEW.document_number = IFNULL(NEW.document_number, CONCAT('FAKTUR', LPAD(@cart_id, 3, '0')));
    -- NEW.document_number = IFNULL(NEW.document_number, CONCAT('FAKTUR', LPAD(IFNULL((SELECT MAX(id) FROM sales_promotion.cart), 0) + 1, 3, '0')));
DROP TRIGGER sales_promotion.cart_document_number;

CREATE 
    TRIGGER  sales_promotion.promotion_document_number
 BEFORE INSERT ON sales_promotion.promotion FOR EACH ROW 
    SET
    -- @last_id = (SELECT MAX(id) FROM sales_promotion.promotion),
    -- @next_id = IFNULL(@last_id, 0) + 1,
    -- NEW.document_number = IFNULL(NEW.document_number, CONCAT('PROMO', LPAD(@next_id, 3, '0')));
    NEW.document_number = IFNULL(NEW.document_number, CONCAT('PROMO', LPAD(IFNULL((SELECT MAX(id) FROM sales_promotion.promotion), 0) + 1, 3, '0')));
DROP TRIGGER sales_promotion.promotion_document_number;
